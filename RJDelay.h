/*
 * RJDelay.h
 *
 * Created: 26.3.2016 18:40:52
 *  Author: Roman J�mbor
 */ 

#ifndef RJDELAY_H
#define RJDELAY_H

#include <util/delay.h>

// Alias pro _ms_delay()
//void sleep(unsigned int ms);
// P�eps�no do macra - nezab�r� pam� nav�c
#define sleep(ms) _delay_ms(ms);

#endif