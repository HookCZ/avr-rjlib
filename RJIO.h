/*
 * RJIO.h
 *
 * Created: 26.3.2016 18:41:09
 *  Author: Roman J�mbor
 */ 

#ifndef RJIO_H
#define RJIO_H

#include <avr/io.h>
//#include "RJPins.h"


// Zjist� logickou �rove� pinu na po�adovan�m portu
#define _isHigh(pin, port) ((port) & (1 << (pin)))
#define isHigh(pin) _isHigh(pin)

// Nastav� pin na dan�m portu na high
#define _high(pin, port) (port) |= 1 << (pin)
#define high(pin) _high(pin)

// Nastav� pin na dan�m portu na low
#define _low(pin, port) (port) &= ~(1 << (pin))
#define low(pin) _low(pin)

// Zm�n� stav pinu na opa�nou hodnotu
#define _toggle(pin, port) (port) ^= 1 << (pin)
#define toggle(pin) _toggle(pin)




#define ADC_REFS_AREF 0
#define ADC_REFS_VCC 1
#define ADC_REFS_2_56 3

#define ADC_ADPS_2 1
#define ADC_ADPS_4 2
#define ADC_ADPS_8 3
#define ADC_ADPS_16 4
#define ADC_ADPS_32 5
#define ADC_ADPS_64 6
#define ADC_ADPS_128 7

// Nastav� ADC
// @param refs {0-3} Posledn� 2 bity ud�vaj� nastaven� pro referen�n� nap�t�
//		0 0 Referen�n� nap�t� na AREF
//		0 1 Referen�n� nap�t� je nap�jec� nap�t�, kondenz�tor na AREF
//		1 0 Reserved
//		1 1 Referen�n� nap�t� je 2,56V, kondenz�tor na AREF
// @param adps {0-7} Posledn� 3 bity ud�vaj� ADC prescale bits
#define setupADC(refs, adps) 	ADMUX = (refs) << REFS0; ADCSRA = ((adps) & 0b00000111) | 0b10000000; // | 0b10000000 => | (1 << ADEN) - zapnut� ADC

// P�e�te hodnotu z dan�ho ADC
// @param {byte} ��slo ADC na MCU odpov�daj�c�m portu
int readADC(uint8_t adcpin);




// Zastav� po�adovan� TCCR - Timer/Counter Control Register
// @param tccr Timer/Counter Control Register
// pozn. TCCR0B
#define stopTimer(tccr) (tccr) &= 0b11111000;

#if defined(COM0A1) && defined(COM0A0)
	// Zastav� porovn�v�n� u po�adovan�ho timeru
	#define removeCompare(tccr, com) (tccr) &= ~(0b00000011 << (com));
	
	// Nastav� porovn�v�n� na po�adovanou hodnotu u po�adovan�ho timeru - p�edpoklad je pr�zdn� compare => nep�ep�e ji� nastaven� compare
	#define setCompare(tccr, com, val) (tccr) |= ((val) & 0b00000011) << (com); //  & 0b00000011 omezuje rozsah na 2 bity
#endif

#ifdef _AVR_ATTINY13A_H_
	// Nastaven� pinu na OUTPUT a zaps�n� comparu pro dan� pin
	#define runPwm0() DDRB |= 0b00000001; setCompare(TCCR0A, COM0A0, 0b00000010);
	#define runPwm1() DDRB |= 0b00000010; setCompare(TCCR0A, COM0B0, 0b00000010);
	
	// Pozastaven� PWM - odebr�n� compare z registru; Lep�� n�zev by byl asi stopPwm, proto�e se v�stup vypne, nehled� na posledn� hodnotu, ale pause l�pe nazna�uje, �e i p�es pozastaven� PWM st�le b�� Timer, kter� je dobr� tak� zastavit, pokud by cht�l �lov�k PWM zcela zastavit
	#define pausePwm0() removeCompare(TCCR0A, COM0A0);
	#define pausePwm1() removeCompare(TCCR0A, COM0B0);
#endif

#define TCCR_CS_0 1
#define TCCR_CS_8 2
#define TCCR_CS_64 3
#define TCCR_CS_256 4
#define TCCR_CS_1024 5

// Nastav� PWM - zapne timer a nastav� po�adovanou frekvenci
// @param cs {0-7} Prescaler - nastaven� frekvence - TCCR_CS_X
#ifdef _AVR_ATTINY13A_H_
	#define setupPWM(cs) TCCR0B |= cs & 0b00000111; /* prescaler*/ TCCR0A |= 0b00000011; //(1 << WGM01) | (1 << WGM00); // Fast PWM
#else
	#define setupPWM(cs) 
	#warning "Pro toto MCU nen� implementovan� setupPWM()"
#endif

// Nastav� po�adovan� PWM na po�adovanou hodnotu
// @param pin {byte} Po�adovan� pin na kter�m m� b�t PWM nastaveno
// @param val {byte} Po�adovan� hodnota
void pwmOut(uint8_t pin, uint8_t val);

#endif