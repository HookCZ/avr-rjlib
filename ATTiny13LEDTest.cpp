/*
 * ATTiny13LEDTest.cpp
 *
 * Created: 17.3.2016 9:28:20
 *  Author: Hawk
 */ 

#define F_CPU 1000000UL // Frekvence MCU

#include "RJIO.h"
#include "RJDelay.h"


// Pojmenov�n� pin�
#define INPT PINB3,PINB
#define LED PINB4,PORTB
#define LED2 PINB0,PORTB


int main(void)
{
	// Nastav�me v�echny porty jako v�stupn� - zamez�me ru�en�
	DDRB = 0b11111111;
	
	// Nastav�me v�echny porty na LOW
	PORTB = 0;
	
	// Port B3 nastav�me na vstup
	DDRB &= ~(1 << PINB3);
	
	
	
	
	// Nastav�me PWM, frekvenci nastav�me na C/8, zapneme timer
	setupPWM(TCCR_CS_8);
	
	// Nastav�me ADC; referen�n� nap�t� je nap�jec� nap�t�; prescaler 2
	setupADC(ADC_REFS_VCC, ADC_ADPS_2);
	
	
	
	
	// Spust�me PWM 0
	runPwm0();
	
	
	// Po��tadla
	//uint8_t c = 0;
	//uint8_t cc = 0;
	
	// Hodnota ADC
	uint8_t ledControlAdcVal = 0;

	while (1) {
		ledControlAdcVal = readADC(3) >> 2; // P�e�te hodnotu z ADC3 a p�evede ji na byte
		pwmOut(PINB0, ledControlAdcVal);
		
		// Cyklov�n� s PWM
		//if (cc == 10 && c == 0) { // Po 10ti cyklech LEDky
			//stopTimer(TCCR0B); // Zastav�me timer
		//} else if (cc == 15 && c == 0) { // Po 15ti cyklech timer op�t zapneme
			//setupPWM(TCCR_CS_8);
		//} else if (cc == 20 && c == 0) { // Po 20ti cyklech vypneme compare na PWM
			//pausePwm0();
		//} else if (cc == 25 && c == 0) { // Po 25ti cyklech PWM op�t zapneme
			//runPwm0();
		//} else {
			//pwmOut(PINB0, c); // Zm�n�me hodnotu (duty) PWM
		//}
		
		//if (c == 255) cc++;
		//c++;
	
		
		// Blik�n� LEDkou sp�n�n�m pinu
		//if (isHigh(INPT)) high(LED);
		//sleep(200);
		//low(LED);
		//sleep(200);
		sleep(10);
	}

	return 0;
}