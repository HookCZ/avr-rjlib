/*
 * RJIO.cpp
 *
 * Created: 26.3.2016 13:55:14
 *  Author: Roman J�mbor
 */ 

#include "RJIO.h"


// P�e�te hodnotu z dan�ho ADC
int readADC(uint8_t adcpin) {
	ADMUX |= adcpin & 0b00000111; // Zapne po�adovan� ADC -> & 7 omezuje hodnotu na posledn� 3 bity
	
	ADCSRA |= 1 << ADSC; // Zap�eme po�adavek na �ten�
	while(ADCSRA & (1<<ADSC)); // Po�k�me, ne� bude ADCS bit op�t 0 - signalizace dokon�en� m��en�
	
	// �ten� v�sledku, je t�eba ��st prvn� ADCL a pot� ADCH - registr se podle toho n�jak lockuje
	uint8_t l, h;
	l = ADCL;
	h = ADCH;
	
	return (h << 8) | l;
}




// Nastav� po�adovan� PWM na po�adovanou hodnotu
// @param pin {byte} Po�adovan� pin na kter�m m� b�t PWM nastaveno
// @param val {byte} Po�adovan� hodnota
void pwmOut(uint8_t pin, uint8_t val) {
	#ifdef _AVR_ATTINY13A_H_ // ATTiny13
		if (pin == 1) {
			OCR0B = val;
		} else if (pin == 0) {
			OCR0A = val;
		}
	#else
		#warning "Pro toto MCU nen� implementovan� pwmOut()"
	#endif
}
